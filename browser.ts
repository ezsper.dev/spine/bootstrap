import { bootstrapHook, dieSync } from '.';

export * from './index';

bootstrapHook.addAction('@spine/bootstrap/death', () => {
  window.addEventListener('beforeunload', () => {
    try {
      dieSync();
    } catch (error) {
      console.error(error.stack || error.message);
    }
  });
}, 0);
