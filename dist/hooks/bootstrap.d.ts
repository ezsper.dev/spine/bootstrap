import { HookAction } from '@spine/hook/HookAction';
declare const _default: HookAction<() => void | Promise<void>>;
export default _default;
