import { HookActionSeries } from '@spine/hook/HookActionSeries';
declare const _default: HookActionSeries<() => void | Promise<void>>;
export default _default;
