import { HookAction } from '@spine/hook/HookAction';
declare const _default: HookAction<(async: boolean) => void | Promise<void>>;
export default _default;
