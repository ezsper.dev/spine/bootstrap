"use strict";

require("core-js/modules/es.array.for-each");

require("core-js/modules/es.object.define-property");

require("core-js/modules/es.object.keys");

require("core-js/modules/web.dom-collections.for-each");

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ = require(".");

var _index = require("./index");

Object.keys(_index).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _index[key];
    }
  });
});

_.bootstrapHook.addAction('@spine/bootstrap/death', function () {
  window.addEventListener('beforeunload', function () {
    try {
      (0, _.dieSync)();
    } catch (error) {
      console.error(error.stack || error.message);
    }
  });
}, 0);
//# sourceMappingURL=browser.js.map