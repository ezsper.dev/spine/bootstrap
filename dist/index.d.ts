export { default as bootstrapHook } from './hooks/bootstrap';
export { default as initHook } from './hooks/init';
export { default as deathHook } from './hooks/death';
export declare const hooks: {
    bootstrap: import("@spine/hook/HookAction").HookAction<() => void | Promise<void>>;
    init: import("@spine/hook/HookActionSeries").HookActionSeries<() => void | Promise<void>>;
    death: import("@spine/hook/HookAction").HookAction<(async: boolean) => void | Promise<void>>;
};
export declare function didDie(): boolean;
export declare function didBootstrap(): boolean;
export declare function bootstrap(): Promise<{
    bootstrap: void;
    init: void;
}>;
export declare function die(): Promise<void>;
export declare function dieSync(): void;
