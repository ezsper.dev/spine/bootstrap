"use strict";

require("core-js/modules/es.array.for-each");

require("core-js/modules/es.object.define-property");

require("core-js/modules/es.object.keys");

require("core-js/modules/web.dom-collections.for-each");

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ = require(".");

var _index = require("./index");

Object.keys(_index).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _index[key];
    }
  });
});

var terminate = function terminate(result) {
  if (result instanceof Error) {
    console.error(result.stack || result.message);
    process.exit(2);
  } else {
    process.exit(0);
  }
};

var died = false;

var dieSafely = function dieSafely() {
  if (!died) {
    died = true;
    (0, _.die)().then(terminate).catch(terminate);
  }
};

_.bootstrapHook.addAction('@spine/bootstrap/death', function () {
  process.on('SIGINT', dieSafely);
  process.on('SIGTERM', dieSafely);
  process.on('exit', dieSafely);
  process.on('uncaughtException', function (err) {
    console.log('UNCAUGHT EXCEPTION');
    console.log("".concat(err.stack || err.message));
    process.exit(2);
  });
}, 0);
//# sourceMappingURL=node.js.map