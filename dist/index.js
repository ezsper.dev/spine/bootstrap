"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

require("core-js/modules/es.object.define-property");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.didDie = didDie;
exports.didBootstrap = didBootstrap;
exports.bootstrap = bootstrap;
exports.die = die;
exports.dieSync = dieSync;
Object.defineProperty(exports, "bootstrapHook", {
  enumerable: true,
  get: function get() {
    return _bootstrap2.default;
  }
});
Object.defineProperty(exports, "initHook", {
  enumerable: true,
  get: function get() {
    return _init.default;
  }
});
Object.defineProperty(exports, "deathHook", {
  enumerable: true,
  get: function get() {
    return _death.default;
  }
});
exports.hooks = void 0;

require("regenerator-runtime/runtime");

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/asyncToGenerator"));

var _promise = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/promise"));

var _bootstrap2 = _interopRequireDefault(require("./hooks/bootstrap"));

var _init = _interopRequireDefault(require("./hooks/init"));

var _death = _interopRequireDefault(require("./hooks/death"));

var hooks = {
  bootstrap: _bootstrap2.default,
  init: _init.default,
  death: _death.default
};
exports.hooks = hooks;
var died = false;

function didDie() {
  return died;
}

var destroy = function destroy() {
  if (didDie()) {
    throw new Error('Dying has already started');
  }

  died = true;
};

var started = false;

function didBootstrap() {
  return started;
}

function bootstrap() {
  return _bootstrap.apply(this, arguments);
}

function _bootstrap() {
  _bootstrap = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee() {
    var bootstrap, init;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            if (!didBootstrap()) {
              _context.next = 2;
              break;
            }

            throw new Error('Bootstrap has already started');

          case 2:
            started = true;
            _context.next = 5;
            return _bootstrap2.default.do();

          case 5:
            bootstrap = _context.sent;
            _context.next = 8;
            return _init.default.do();

          case 8:
            init = _context.sent;
            return _context.abrupt("return", {
              bootstrap: bootstrap,
              init: init
            });

          case 10:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _bootstrap.apply(this, arguments);
}

function die() {
  return _die.apply(this, arguments);
}

function _die() {
  _die = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee2() {
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            destroy();
            return _context2.abrupt("return", _death.default.do(true));

          case 2:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));
  return _die.apply(this, arguments);
}

function dieSync() {
  destroy();

  _death.default.do(false).catch(function (error) {
    throw error;
  });
}
//# sourceMappingURL=index.js.map