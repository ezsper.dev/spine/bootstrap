import { default as bootstrapHook } from './hooks/bootstrap';
import { default as initHook } from './hooks/init';
import { default as deathHook } from './hooks/death';

export { default as bootstrapHook } from './hooks/bootstrap';

export { default as initHook } from './hooks/init';
export { default as deathHook } from './hooks/death';

export const hooks = {
  bootstrap: bootstrapHook,
  init: initHook,
  death: deathHook,
};

let died = false;

export function didDie() {
  return died;
}

const destroy = () => {
  if (didDie()) {
    throw new Error('Dying has already started');
  }
  died = true;
};

let started = false;

export function didBootstrap() {
  return started;
}

export async function bootstrap() {
  if (didBootstrap()) {
    throw new Error('Bootstrap has already started');
  }
  started = true;
  const bootstrap = await bootstrapHook.do();
  const init = await initHook.do();
  return {
    bootstrap,
    init,
  };
}

export async function die(): Promise<void> {
  destroy();
  return deathHook.do(true);
}

export function dieSync(): void {
  destroy();
  deathHook.do(false)
    .catch(error => {
      throw error;
    });
}
