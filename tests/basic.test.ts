/// <reference types="jest" />
import {
  deathHook,
  initHook,
  bootstrapHook,
  bootstrap,
} from '../index';

describe('Bootstrap', () => {

  it('Init hook', async () => {
    let value = 0;
    initHook.addAction('test', () => {
      value += 1;
    });
    await bootstrap();
    expect(value).toBe(1);
  });

});
