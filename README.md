# Spine Bootstrap

This module will make sure your entire project
is pluggable.

> This is an additional dependency for [Spine Hook](https://gitlab.com/ezsper.com/spine/hook)

## Usage

### Create your hook

```
# myPlugin.ts
import {
  bootstrapHook,
  initHook,
  deathHook,
} from '@spine/bootstrap';

bootstrapHook.addAction('your-plugin', () => {
  /* This is an opportunite to bind or unbind any plugin */
  console.log('bootstrap');
});

initHook.addAction('your-plugin', async () => {
  /* Initialize something  */
  console.log('executed after bootstrap hook');
});

deathHook.addAction('your-plugin', async () => {
  /* It will execute when window unloads or process is killed */
  console.log('destroy');
});
```

### Bootstrap your project
```
# index.ts
import { bootstrap } from '@spine/bootstrap/server';

// load your plugins
import './myPlugin';

bootstrap()
  .catch(error => console.log(error.stack || error.message));
```
