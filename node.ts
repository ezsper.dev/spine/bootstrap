import { bootstrapHook, die } from '.';

export * from './index';

const terminate = (result: any) => {
  if (result instanceof Error) {
    console.error(result.stack || result.message);
    process.exit(2);
  } else {
    process.exit(0);
  }
};

let died = false;
const dieSafely = () => {
  if (!died) {
    died = true;
    die()
      .then(terminate)
      .catch(terminate);
  }
};

bootstrapHook.addAction('@spine/bootstrap/death', () => {
  process.on('SIGINT', dieSafely);
  process.on('SIGTERM', dieSafely);
  process.on('exit', dieSafely);
  process.on('uncaughtException', (err) => {
    console.log('UNCAUGHT EXCEPTION');
    console.log(`${err.stack || err.message}`);
    process.exit(2);
  });
}, 0);
